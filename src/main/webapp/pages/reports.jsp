<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta charset="utf-8">
		<meta name="contextPath" content="${contextPath}">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta name="viewport" content="width=device-width" /> 	
	 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 	
	 	<title>LiveIt Merit System</title>
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
		<link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/adminStyle.css" />
		<link rel="stylesheet" href="../css/skin-red.min.css" />
		<link rel="stylesheet" href="../css/styles.css" />
		
		<link rel="stylesheet" href="../css/plugins/dataTables/buttons.dataTables.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.responsive.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.tableTools.min.css">
		<link rel="stylesheet" href="../lib/css/bootstrap-datepicker.min.css" />
	</head>
	<body class="hold-transition skin-red sidebar-mini">
		<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="modalGenerateReport">
		    <div class="modal-dialog modal-sm" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="modalGenerateReport">Generate Report</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="alert-here"></div>
		                <div class="input-daterange" id="datepicker">
		                	<label>From <small><em>(Date Redeemed)</em></small></label>
							<div class="input-group form-group">
								<span class="input-group-addon">
	                            	<i class="fa fa-calendar-o" aria-hidden="true"></i>
								</span>
                            	<input type="text" class="form-control" name="start_date" required placeholder="Start date">
                            </div>
                            <label>To <small><em>(Date Redeemed)</em></small></label>
                            <div class="input-group form-group">
								<span class="input-group-addon">
	                            	<i class="fa fa-calendar-o" aria-hidden="true"></i>
								</span>
                            	<input type="text" class="form-control" name="end_date" required placeholder="End date">
                            </div>
                        </div>
		                <button class="lms-btn pull-right" id="btnGenReport">
		                	Generate Report
		                </button>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		    </div>
		</div>
		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo">
					<span class="logo-mini"><b>L</b>IT</span>
					<span class="logo-lg"><b>Live</b>IT</span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        				<span class="sr-only">Toggle navigation</span>
      				</a>
      				<div class="navbar-custom-menu">
      					<ul class="nav navbar-nav">
      						<li class="dropdown user user-menu">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				              <span class="fa fa-user"></span>
				              <span class="hidden-xs">Admin</span>
				            </a>
				            <ul class="dropdown-menu">
				              <!-- The user image in the menu -->
				              <li class="user-header">
				                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
				
				                <p>
				                  Admin - Administrator
				                  <small>Member since Nov. 2012</small>
				                </p>
				              </li>
				              <li class="user-footer">
				                <div class="pull-left">
				                  <a href="#" class="btn btn-default btn-flat">Profile</a>
				                </div>
				                <div class="pull-right">
				                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
				                </div>
				              </li>
				            </ul>
				          </li>
      					</ul>
      				</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
				      <ul class="sidebar-menu">
				      	<li class="header">Menu</li>
				        <li><a href="/SoaBaseCode/admin/users"><i class="fa fa-table"></i> <span>User list</span></a></li>
				        <li><a href="/SoaBaseCode/admin/merits"><i class="fa fa-check-square-o"></i> <span>Merits</span></a></li>
				        <li><a href="/SoaBaseCode/admin/rewards"><i class="fa fa-star"></i> <span>Rewards</span></a></li>
				        <li class="active"><a href="/SoaBaseCode/admin/reports"><i class="fa fa-database" aria-hidden="true"></i> <span>Report</span></a></li>
				      </ul>
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Redemption Report<small></small></h1>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header with-border">
							 <h3 class="box-title">Redemption List Table</h3>
							 <div class="box-tools pull-right">
           						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              					<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<form>
								<div class="form-group input-group pull-left" style='width: 250px'>
									<input type="text" class="form-control" name="q" placeholder="Search redemption list">
									<span class="input-group-addon">
										<button id="btnSearch" type="submit" style="background-color: transparent; border: 0px;"><i class="fa fa-search" aria-hidden="true"></i></button>
									</span>
								</div>
							</form>
							<div class="lms-action pull-right form-group">
								<button class="btn btn-info" data-toggle="modal" data-target="#reportModal">
									<i class="fa fa-calendar" aria-hidden="true"></i> &nbsp; Report by Dates
								</button>
								<button class="btn btn-danger" id="btnGeneratePDF">
									<i class="fa fa-file-pdf-o" aria-hidden="true"></i> &nbsp; Generate PDF
								</button>
							</div>
							<div class="row">
								<div class="col-md-12">
									<table id="tblRedemption" class="table table-striped table-bordered table-hover">
              							<thead>
              								<tr>
              									<th>ID</th>
              									<th>Redeemed By</th>
              									<th>Reward Redeemed</th>
              									<th>Date Redeemed</th>
              									<th>Points Required</th>
              									<th>Quantity</th>
              								</tr>
              							</thead>
              							<tbody>
              								<c:forEach items="${redeemedList}" var="rr">
											    <tr>
											        <td><c:out value="${rr.rrId}"/></td>
											        <td><c:out value="${rr.user.lname}, ${rr.user.fname}"/></td>
											        <td><c:out value="${rr.reward.rewardDesc}"/></td>
											        <td><fmt:formatDate value="${rr.dateRedeemed}" pattern="MM/dd/yyyy" /></td>
											        <td><c:out value="${rr.pointsReq}"/></td>
											        <td><c:out value="${rr.qty}"/></td>
											    </tr>
											</c:forEach>
              							</tbody>
              						</table>
              						<c:if test="${q != null}">
              							<button class="lms-btn" id="btnShowAll" style="padding: 3px 8px;"><i class="fa fa-refresh" aria-hidden="true"></i> &nbsp; Show all Redeemed Rewards</button> &nbsp;
              							<c:if test="${length == 0}">
              								<span class="result-here">No data found for the keyword <em>"${q}"</em>.</span>
              							</c:if>
              							<c:if test="${length == 1}">
              								<span class="result-here">Found ${length} result for the keyword <em>"${q}"</em>.</span>
              							</c:if>
              							<c:if test="${length > 1}">
              								<span class="result-here">Found ${length} results for the keyword <em>"${q}"</em>.</span>
              							</c:if>
              						</c:if>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
			    <div class="pull-right hidden-xs">LiveIt-Merit-Reward System</div>
			    <strong>Copyright &copy; 2016 <a href="#">UC-MAIN Campus</a>.</strong> All rights reserved.
			</footer>	
	</div>
		
	<script src="../js/jquery-2.2.3.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/app.js"></script>
	<script src="../lib/js/bootstrap-datepicker.min.js"></script>
	<script src="../lib/js/moment.js"></script>
	<script src="../lib/js/jspdf.min.js"></script>
	<script src="../lib/js/jspdf.plugin.autotable.min.js"></script>
	
	<!-- Datatables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
	<script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>
	
	<script>
		var ROOT_URL = "http://localhost:8080/SoaBaseCode/";
		$('.input-daterange').datepicker({container:'#reportModal'});
		
		function showMsgAlert(cls, msg, parentDiv) {
			var _cls = "danger";
			var _xprs = "Oops!"
			if(cls) {
				_cls = "success";
				_xprs = "Oops!"
			}
			var c = "<div class='alert alert-"+_cls+" alert-dismissable'>"+
						"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
				 		"<strong>"+_xprs+"</strong> "+msg+
					"</div>";
		 	$("#"+parentDiv+" .alert-here").html($(c));
		}
		
		function clearReportModal() {
			$("input[name=start_date]", "input[name=end_date]").val("");
			$("#reportModal .alert-here").html("");
		}
		
		$(document).on("click", "#btnGenReport", function() {
			var start_date = $("input[name=start_date]").val().trim();
			if(!start_date) {
				showMsgAlert(false, "Please fill start date.", "reportModal");
				return false;
			}
			
			var end_date = $("input[name=end_date]").val().trim();
			if(!end_date) {
				showMsgAlert(false, "Please fill end date.", "reportModal");
				return false;
			}
			
			
			$.get(ROOT_URL + "admin/api/get-redeemed-list-by-dates",
				{
					startDate: start_date,
					endDate: end_date
				},
				function(response) {
					if(!response.length) {
						showMsgAlert(false, "No results found", "reportModal");						
					} else {
						$("#tblRedemption tbody").html("");
						$.each(response, function(idx, value) {
							var tr = "<tr>";
									tr += "<td>"+value.rrId+"</td>"
									tr += "<td>"+value.user.lname+", "+value.user.fname+"</td>"
									tr += "<td>"+value.reward.rewardDesc+"</td>"
									tr += "<td>"+moment(value.dateRedeemed).format("L")+"</td>"
									tr += "<td>"+value.pointsReq+"</td>"
									tr += "<td>"+value.qty+"</td>"
								tr += "</tr>";
							$("#tblRedemption tbody").append($(tr));
						});
						clearReportModal();
						$("#reportModal").modal("hide");
					}
				}
			);	
		});

		$(document).on("click", "#btnGeneratePDF", function() {
		    var doc = new jsPDF();
		    doc.setFontSize(14);
		    doc.setFontType("bold");
		    doc.text("LiveIT Merit System", 14, 16);
		    doc.setFontType("normal");
		    doc.setFontSize(12);
		    doc.text("Redemption Report", 14, 20);
		    var elem = document.getElementById("tblRedemption");
		    var res = doc.autoTableHtmlToJson(elem);
		    doc.autoTable(res.columns, res.data, {startY: 25});
		    doc.save("report.pdf");
		});
		
		$(document).on("click", "#btnShowAll", function() {
			$("input[name=q]").val("");
			$("#btnSearch").trigger("click");
		});
	</script>
	</body>
</html>