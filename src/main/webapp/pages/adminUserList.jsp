<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="utf-8">
<meta name="contextPath" content="${contextPath}">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width" />
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>LiveIt Merit System</title>
<link rel="stylesheet" href="../css/font-awesome.min.css" />
<link rel="stylesheet" href="../css/bootstrap.min.css" />
<link rel="stylesheet" href="../css/adminStyle.css" />
<link rel="stylesheet" href="../css/skin-red.min.css" />
<link rel="stylesheet"
	href="../css/plugins/dataTables/buttons.dataTables.min.css" />
<link rel="stylesheet"
	href="../css/plugins/dataTables/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="../css/plugins/dataTables/dataTables.responsive.css">
<link rel="stylesheet"
	href="../css/plugins/dataTables/dataTables.tableTools.min.css">
<link rel="stylesheet" href="../lib/css/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="../css/styles.css" />
</head>
<div class="modal fade" tabindex="-1" role="dialog" id="addUserModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="/SoaBaseCode/admin/users/create">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Add another user</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>First name</label> <input type="text" class="form-control"
							name="fname" placeholder="First name">
					</div>
					<div class="form-group">
						<label>Middle name</label> <input type="text" class="form-control"
							name="mname" placeholder="Middle name">
					</div>
					<div class="form-group">
						<label>Last name</label> <input type="text" class="form-control"
							name="lname" placeholder="Last name">
					</div>
					<div class="form-group">
						<label>Email</label> <input type="email" class="form-control"
							name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label>Birth date</label> <input type="text" class="form-control"
							name="bDate" placeholder="Birth date (mm/dd/yyyy)">
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div>
									<label>Gender</label>
								</div>
								<label> <input type="radio" name="gender" value="Male">
									&nbsp; Male
								</label> &nbsp; <label> <input type="radio" name="gender"
									value="Female"> &nbsp; Female
								</label>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div>
									<label>Role</label>
								</div>
								<label> <input type="radio" name="role" value="USER">
									&nbsp; Normal User
								</label> &nbsp; <label> <input type="radio" name="role"
									value="PL"> &nbsp; Project Leader
								</label> <input type="hidden" name="status" value="ACTIVE"> <input
									type="hidden" name="password" value="123456">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteUserModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="/SoaBaseCode/admin/users/delete">
				<input type="hidden" name="userId">
				<input type="hidden" name="role">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Deactivate user?</h4>
				</div>
				<div class="modal-body">
					<span>Do you want to deactivate <span class="userEmail"></span>?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="activateUserModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="/SoaBaseCode/admin/users/activate">
				<input type="hidden" name="userId">
				<input type="hidden" name="role">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Activate user!</h4>
				</div>
				<div class="modal-body">
					<span>Do you want to activate <span class="userEmail"></span>?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Activate</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="updateUserModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="/SoaBaseCode/admin/users/update">
				<input type="hidden" name="userId">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Update user</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>First name</label> <input type="text" class="form-control"
							name="fname" placeholder="First name" value="hey">
					</div>
					<div class="form-group">
						<label>Middle name</label> <input type="text" class="form-control"
							name="mname" placeholder="Middle name">
					</div>
					<div class="form-group">
						<label>Last name</label> <input type="text" class="form-control"
							name="lname" placeholder="Last name">
					</div>
					<div class="form-group">
						<label>Email</label> <input type="email" class="form-control"
							name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label>Birth date</label> <input type="text" class="form-control"
							name="bDate" placeholder="Birth date (mm/dd/yyyy)">
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<div>
									<label>Gender</label>
								</div>
								<label> <input type="radio" name="gender" value="Male">
									&nbsp; Male
								</label> &nbsp; <label> <input type="radio" name="gender"
									value="Female"> &nbsp; Female
								</label>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div>
									<label>Role</label>
								</div>
								<label> <input type="radio" name="role" value="USER">
									&nbsp; Normal User
								</label> &nbsp; <label> <input type="radio" name="role"
									value="PL"> &nbsp; Project Leader
								</label> <input type="hidden" name="status" value="ACTIVE"> <input
									type="hidden" name="password" value="123456">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<body class="hold-transition skin-red sidebar-mini">
	<div class="wrapper">
		<header class="main-header"> <a href="#" class="logo"> <span
			class="logo-mini"><b>L</b>IT</span> <span class="logo-lg"><b>Live</b>IT</span>
		</a> <nav class="navbar navbar-static-top" role="navigation"> <a
			href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <span
						class="fa fa-user"></span> <span class="hidden-xs">${user.fname}</span>
				</a>
					<ul class="dropdown-menu">
						<!-- The user image in the menu -->
						<li class="user-header">
							<p>
								Admin - Administrator <small>Member since Nov. 2012</small>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="#" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul></li>
			</ul>
		</div>
		</nav> </header>
		<aside class="main-sidebar"> <section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">Menu</li>
			<li class="active"><a href="/SoaBaseCode/admin/users"><i
					class="fa fa-table"></i> <span>Users</span></a></li>
			<li><a href="/SoaBaseCode/admin/merits"><i
					class="fa fa-check-square-o"></i> <span>Merits</span></a></li>
			<li><a href="/SoaBaseCode/admin/rewards"><i
					class="fa fa-star"></i> <span>Rewards</span></a></li>
			<li><a href="/SoaBaseCode/admin/report"><i
					class="fa fa-database" aria-hidden="true"></i> <span>Report</span></a></li>
		</ul>
		</section> </aside>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				User list<small></small>
			</h1>
			</section>
			<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">User list table</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool"
							data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<c:if test="${flag == 'SUCCESS'}">
						<div class="alert alert-success alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> User added successfully.
						</div>
					</c:if>
					<c:if test="${flag == 'EDITED'}">
						<div class="alert alert-success alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> User updated successfully.
						</div>
					</c:if>
					<c:if test="${flag == 'DELETED'}">
						<div class="alert alert-success alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> User deleted successfully.
						</div>
					</c:if>
					<c:if test="${flag == 'ACTIVATED'}">
						<div class="alert alert-success alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Success!</strong> User activated successfully.
						</div>
					</c:if>
					<c:if test="${flag == 'FAILED'}">
						<div class="alert alert-danger alert-dismissable">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<strong>Oops!</strong> Failed to add user.
						</div>
					</c:if>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group input-group pull-left"
								style='width: 250px'>
								<input type="text" class="form-control" name="q"
									placeholder="Search last name"> <span
									class="input-group-addon"><i class="fa fa-search"
									aria-hidden="true"></i></span>
							</div>
							<button class="lms-btn pull-right form-group" data-toggle="modal"
								data-target="#addUserModal">
								<i class="fa fa-user-plus" aria-hidden="true"></i> Add user
							</button>
							<div class="clearfix"></div>
							<table id="example2"
								class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>ID</th>
										<th>First name</th>
										<th>Middle name</th>
										<th>Last name</th>
										<th>Email</th>
										<th>Gender</th>
										<th>Birthdate</th>
										<th>Role</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${userList}" var="u">
										<tr id="${u.userId}">
											<td><c:out value="${u.userId}" /></td>
											<td><c:out value="${u.fname}" /></td>
											<td><c:out value="${u.mname}" /></td>
											<td><c:out value="${u.lname}" /></td>
											<td><c:out value="${u.email}" /></td>
											<td><c:out value="${u.gender}" /></td>
											<td><c:out value="${u.bDate}" /></td>
											<td><c:out value="${u.role}" /></td>
											<td><c:out value="${u.status}" /></td>
											<td>
												<button class="btn btn-success btn-edit-user"
													data-toggle="modal" data-target="#updateUserModal">
													<i class="fa fa-pencil" aria-hidden="true"></i>
												</button> 
												<c:if test="${u.status == 'ACTIVE'}">
													<button class="btn btn-danger btn-delete-user"
														data-toggle="modal" data-target="#deleteUserModal">
														<i class="fa fa-trash" aria-hidden="true"></i>
													</button>
												</c:if>
												<c:if test="${u.status == 'INACTIVE'}">
													<button class="btn btn-warning btn-activate-user"
														data-toggle="modal" data-target="#activateUserModal">
														<i class="fa fa-level-up" aria-hidden="true"></i>
													</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
		<footer class="main-footer">
		<div class="pull-right hidden-xs">LiveIT Merit Reward System</div>
		<strong>Copyright &copy; 2016 <a href="#">UC-MAIN Campus</a>.
		</strong> All rights reserved. </footer>
	</div>

	<script src="../js/jquery-2.2.3.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/app.js"></script>
	<script src="../lib/js/bootstrap-datepicker.min.js"></script>


	<!-- Datatables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
	<script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>

	<script type="text/javascript">
		$("input[name=bDate]").datepicker({
			container : "#addUserModal"
		});

		$(document).on("click",".btn-edit-user",function() {
					var tr = $(this).parents("tr");
					var td = tr.find("td");
					$("#updateUserModal input[name=userId]").val($(td[0]).text());
					$("#updateUserModal input[name=fname]").val($(td[1]).text());
					$("#updateUserModal input[name=mname]").val($(td[2]).text());
					$("#updateUserModal input[name=lname]").val($(td[3]).text());
					$("#updateUserModal input[name=email]").val($(td[4]).text());
					$("#updateUserModal input[value=" + $(td[5]).text() + "]").val($(td[5]).text()).attr('checked', true);
					$("#updateUserModal input[name=bDate]").val($(td[6]).text());
					$("#updateUserModal input[value=" + $(td[7]).text() + "]").val($(td[7]).text()).attr('checked', true);
					$("#updateUserModal input[name=status]").val($(td[8]).text());
				});
		$(document).on("click", ".btn-delete-user", function() {
			var tr = $(this).parents("tr");
			var td = tr.find("td");
			$("#deleteUserModal input[name=userId]").val($(td[0]).text());
			$("#deleteUserModal input[name=email]").val($(td[4]).text());
			$("#deleteUserModal .userEmail").text($(td[4]).text());
			$("#deleteUserModal input[name=role]").val($(td[7]).text());
	
		});
		$(document).on("click", ".btn-activate-user", function() {
			var tr = $(this).parents("tr");
			var td = tr.find("td");
			$("#activateUserModal input[name=userId]").val($(td[0]).text());
			$("#activateUserModal input[name=email]").val($(td[4]).text());
			$("#activateUserModal .userEmail").text($(td[4]).text());
			$("#activateUserModal input[name=role]").val($(td[7]).text());
	
		});
	</script>
</body>
</html>