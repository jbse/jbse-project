<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
 <title>LiveIT Merit System | Login</title>
 	<link rel="stylesheet" href="lib/css/font-awesome.min.css" />
	<link rel="stylesheet" href="lib/css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/media-queries.css" />
	<link rel="stylesheet" href="css/styles.css" />
  	<meta name="viewport" content="width=device-width" /> 	
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
<body>
	<nav class="navbar navbar-default">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">LiveIT Merit System</a>
		</div>
	</nav>
	<div class="navbar-bottom"></div>
	<form method="post" action="/SoaBaseCode/api/login" autocomplete="off">
		<div class="login-section">
			<div class="login-wrapper">
				<h2 class='lms-sign-in-title'>Sign-in</h2>
				<hr/>
				<div class="lms-form">
					<h5>Sign in to LiveIT Merit System to get started.</h5>
					<div class="form-group">
						<span class="lms-error">
							${error}
						</span>
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<label>
								<i class="fa fa-user" aria-hidden="true"></i>
							</label>
						</span>
						<input type="email" class="form-control input-lg" name="email" required placeholder="Email">
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">
							<label>
								<i class="fa fa-lock" aria-hidden="true"></i>
							</label>
						</span>
						<input type="password" class="form-control input-lg" name="password" required placeholder="Password">
					</div>
					<button type="submit" id="btnLogin" class="lms-btn">Sign in</button>
				</div>
			</div>
		</div>
	</form>
</body>
	<script src="lib/js/jquery-1.10.1.min.js"></script>
	<script src="lib/js/bootstrap.min.js"></script>
	<script src="js/common.js"></script>
	<script src="lib/js/jquery.blockUI.js"></script>
</html>