<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta charset="utf-8">
		<meta name="contextPath" content="${contextPath}">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta name="viewport" content="width=device-width" /> 	
	 	<meta content="width=device-width, initial-scale=s1, maximum-scale=1, user-scalable=no" name="viewport">
	 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 	
	 	<title>LiveIt Merit System</title>
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
		<link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/adminStyle.css" />
		<link rel="stylesheet" href="../css/skin-red.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/buttons.dataTables.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.responsive.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.tableTools.min.css">
		<link rel="stylesheet" href="../lib/css/bootstrap-datepicker.min.css" />
		<link rel="stylesheet" href="../css/styles.css" />
	</head>
	<body class="hold-transition skin-red sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo">
					<span class="logo-mini"><b>L</b>IT</span>
					<span class="logo-lg"><b>Live</b>IT</span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        				<span class="sr-only">Toggle navigation</span>
      				</a>
      				<div class="navbar-custom-menu">
      					<ul class="nav navbar-nav">
      						<li class="dropdown user user-menu">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				              <span class="fa fa-user"></span>
				              <span class="hidden-xs">${user.fname}</span>
				            </a>
				            <ul class="dropdown-menu">
				              <!-- The user image in the menu -->
				              <li class="user-header">
				                <p>
				                  Admin - Administrator
				                  <small>Member since Nov. 2012</small>
				                </p>
				              </li>
				              <li class="user-footer">
				                <div class="pull-left">
				                  <a href="#" class="btn btn-default btn-flat">Profile</a>
				                </div>
				                <div class="pull-right">
				                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
				                </div>
				              </li>
				            </ul>
				          </li>
      					</ul>
      				</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
				      <ul class="sidebar-menu">
				      	<li class="header">Menu</li>
				      	<li class="active"><a href="/SoaBaseCode/pl/home"><i class="fa fa-home"></i> <span>Home</span></a></li>
				        <li class=""><a href="/SoaBaseCode/admin/users"><i class="fa fa-users"></i> <span>Member</span></a></li>
				        <li><a href="/SoaBaseCode/admin/report"><i class="fa fa-star" aria-hidden="true"></i> <span>Redeem points</span></a></li>
				      </ul>
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>User Profile<small></small></h1>
				</section>
				<section class="content">
					<div class="col-md-4">
			          <!-- Widget: user widget style 1 -->
			          <div class="box box-widget widget-user">
			            <!-- Add the bg color to the header using any of the bg-* classes -->
			            <div class="widget-user-header bg-aqua-active">
			              <h3 class="widget-user-username">Alexander Pierce</h3>
			              <h5 class="widget-user-desc">Founder &amp; CEO</h5>
			            </div>
			            <div class="widget-user-image">
			              <img src="<c:url value='../css/user.png'/>" class="img-circle" alt="User Avatar">
			            </div>
			            <div class="box-footer">
			              <div class="row">
			                <div class="col-sm-4 border-right">
			                  <div class="description-block">
			                    <h5 class="description-header">3,200</h5>
			                    <span class="description-text">POINTS</span>
			                  </div>
			                  <!-- /.description-block -->
			                </div>
			                <!-- /.col -->
			                <!-- /.col -->
			                <div class="col-sm-4">
			                  <div class="description-block">
			                    <h5 class="description-header">35</h5>
			                    <span class="description-text">MEMBERS</span>
			                  </div>
			                  <!-- /.description-block -->
			                </div>
			                <!-- /.col -->
			              </div>
			              <!-- /.row -->
			            </div>
			          </div>
			          <!-- /.widget-user -->
       			 </div>
       			 <div class="col-md-8">
       			 	<div class="box">
       			 		<div class="box-header">
       			 			<h4>Members</h4>
       			 		</div>
       			 		<div class="box-content">
       			 			<table class="table table-bordered">
       			 				<tr>
       			 					<td>SAMPLE</td>
       			 					<td width="10%"><button class="btn btn-primary">Add merit</button></td>
       			 				</tr>
       			 			</table>
       			 		</div>
       			 	</div>
       			 </div>
				</section>
			</div>
			<footer class="main-footer">
			    <div class="pull-right hidden-xs">LiveIT Merit Reward System</div>
			    <strong>Copyright &copy; 2016 <a href="#">UC-MAIN Campus</a>.</strong> All rights reserved.
			</footer>	
	</div>
	
	<script src="../js/jquery-2.2.3.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/app.js"></script>
	<script src="../lib/js/bootstrap-datepicker.min.js"></script>
	
	
	<!-- Datatables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
	<script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>
	
	<script type="text/javascript">
		$("input[name=bDate]").datepicker({
			container: "#addUserModal"
		});
	</script>
	</body>
</html>