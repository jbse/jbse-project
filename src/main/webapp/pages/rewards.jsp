<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta charset="utf-8">
		<meta name="contextPath" content="${contextPath}">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta name="viewport" content="width=device-width" /> 	
	 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 	
	 	<title>LiveIt Merit System | Rewards</title>
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
		<link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/adminStyle.css" />
		<link rel="stylesheet" href="../css/skin-red.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/buttons.dataTables.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.responsive.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.tableTools.min.css">
		<link rel="stylesheet" href="../lib/css/bootstrap-datepicker.min.css" />
		<link rel="stylesheet" href="../css/styles.css" />
	</head>
	<div class="modal fade" tabindex="-1" role="dialog" id="addRewardModal">
	    <div class="modal-dialog" role="document">
			<div class="modal-content">
					<form method="post">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Add another reward</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="form-group">
							<label>Reward Description</label>
				            <textarea class="form-control" name="rewardDesc" placeholder="Reward description..."></textarea>
			            </div>
			            <div class="form-group">
							<label>Points Required</label>
				            <input type="number" class="form-control" name="pointsReq" min="0" step="1" placeholder="Reward points required">
			            </div>
			            <div class="form-group">
							<label>Quantity</label>
				            <input type="number" class="form-control" name="qty" min="0" step="1" placeholder="Quantity">
			            </div>
			            <input type="hidden" name="status" value="AVAILABLE" readonly>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary">Save changes</button>
		            </div>
	            </form>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
<!-- /.modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="updateRewardModal">
	    <div class="modal-dialog" role="document">
			<div class="modal-content">
					<form method="post" action="/SoaBaseCode/admin/rewards/updateReward">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Edit reward</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="form-group">
							<label>Reward Description</label>
				            <textarea class="form-control" name="rewardDesc" placeholder="Reward description..." id="rewardDesc"></textarea>
			            </div>
			            <input type="hidden" name="rewardId" >
			            <div class="form-group">
							<label>Points Required</label>
				            <input type="number" class="form-control" id="pointsReq" name="pointsReq" min="0" step="1" placeholder="Reward points required">
			            </div>
			            <div class="form-group">
							<label>Quantity</label>
				            <input type="number" class="form-control" id="qty" name="qty" min="0" step="1" placeholder="Quantity">
			            </div>
			            <input type="hidden" name="status" value="AVAILABLE" readonly>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary">Save changes</button>
		            </div>
	            </form>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
<!-- /.modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="deleteRewardModal">
	    <div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/SoaBaseCode/admin/rewards/deleteReward" >
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Delete reward</h4>
		            </div>
		            <div class="modal-body">
		            	<input type="hidden" name="rewardId" >
		            	Are you sure you want to delete this reward?
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary" id="btnConfirmDelete">Delete</button>
		            </div>
	            </form>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
<!-- /.modal -->

	<body class="hold-transition skin-red sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo">
					<span class="logo-mini"><b>L</b>IT</span>
					<span class="logo-lg"><b>Live</b>IT</span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        				<span class="sr-only">Toggle navigation</span>
      				</a>
      				<div class="navbar-custom-menu">
      					<ul class="nav navbar-nav">
      						<li class="dropdown user user-menu">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				              <span class="fa fa-user"></span>
				              <span class="hidden-xs">${user.fname}</span>
				            </a>
				            <ul class="dropdown-menu">
				              <!-- The user image in the menu -->
				              <li class="user-header">
				                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
				
				                <p>
				                  Admin - Administrator
				                  <small>Member since Nov. 2012</small>
				                </p>
				              </li>
				              <li class="user-footer">
				                <div class="pull-left">
				                  <a href="#" class="btn btn-default btn-flat">Profile</a>
				                </div>
				                <div class="pull-right">
				                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
				                </div>
				              </li>
				            </ul>
				          </li>
      					</ul>
      				</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
				      <ul class="sidebar-menu">
				      	<li class="header">Menu</li>
				        <li><a href="/SoaBaseCode/admin/users"><i class="fa fa-table"></i> <span>Users</span></a></li>
				        <li><a href="/SoaBaseCode/admin/merits"><i class="fa fa-check-square-o"></i> <span>Merits</span></a></li>
				        <li class="active"><a href="/SoaBaseCode/admin/rewards"><i class="fa fa-star"></i> <span>Rewards</span></a></li>
				        <li><a href="/SoaBaseCode/admin/report"><i class="fa fa-database" aria-hidden="true"></i> <span>Report</span></a></li>
				      </ul>
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Reward list<small></small></h1>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header with-border">
							 <h3 class="box-title">Reward list table</h3>
							 <div class="box-tools pull-right">
           						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              					<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<c:if test="${flag == 'SUCCESS'}">
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> Reward added successfully.
								</div>
							</c:if>
							<c:if test="${flag == 'FAILED'}">
								<div class="alert alert-danger alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Oops!</strong> Failed to add reward.
								</div>
							</c:if>
							<c:if test="${flag == 'UPDATED'}">
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> Reward updated successfully.
								</div>
							</c:if>
							<c:if test="${flag == 'DELETED'}">
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> Reward deleted successfully.
								</div>
							</c:if>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group input-group pull-left" style='width: 250px'>
										<input type="text" class="form-control" name="q" placeholder="Search by id">
										<span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
									</div>
									<button class="lms-btn pull-right form-group" data-toggle="modal" data-target="#addRewardModal">
										<i class="fa fa-plus" aria-hidden="true"></i> Add reward
									</button>
									<div class="clearfix"></div>
									<table id="example2" class="table table-striped table-bordered table-hover">
              							<thead>
              								<tr>
              									<th>ID</th>
              									<th>Description</th>
              									<th>Points Required</th>
              									<th>Quantity Available</th>
              									<th>Status</th>
              								</tr>
              							</thead>
              							<tbody>
											<c:forEach items="${rewardList}" var="r">
											    <tr id="${r.rewardId}">
											        <td><c:out value="${r.rewardId}"/></td>
											        <td><c:out value="${r.rewardDesc}"/></td>
											        <td><c:out value="${r.pointsReq}"/></td>
											        <td><c:out value="${r.qty}"/></td>
											        <td><c:out value="${r.status}"/></td>
											        <td>
											        	<button class="btn btn-success updateRewardModal" data-toggle="modal" data-target="#updateRewardModal">
											        		<i class="fa fa-pencil" aria-hidden="true"></i>
											        	</button>											        	
											        	<button class="btn btn-danger btn-delete" data-toggle="modal" data-target="#deleteRewardModal">
											        		<i class="fa fa-trash" aria-hidden="true"></i>
											        	</button>
											        </td>
											    </tr>
											</c:forEach>
              							</tbody>
              						</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
			    <div class="pull-right hidden-xs">LiveIT Merit Reward System</div>
			    <strong>Copyright &copy; 2016 <a href="#">UC-MAIN Campus</a>.</strong> All rights reserved.
			</footer>	
	</div>
	
	<script src="../js/jquery-2.2.3.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/app.js"></script>
	<script src="../lib/js/bootstrap-datepicker.min.js"></script>
	
	
	<!-- Datatables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
	<script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>
	
	<script type="text/javascript">
		$(document).on("click", ".updateRewardModal", function(){
			var tr = $(this).parents("tr");
			var td = tr.find("td");	
			$("#updateRewardModal input[name=rewardId]").val($(td[0]).text());
			$("#updateRewardModal textarea[name=rewardDesc]").val($(td[1]).text());
			$("#updateRewardModal input[name=pointsReq]").val($(td[2]).text());
			$("#updateRewardModal input[name=qty]").val($(td[3]).text());
			$("#updateRewardModal input[name=status]").val($(td[4]).text());
		});
		
		$(document).on("click", ".btn-delete", function(){
			var tr = $(this).parents("tr");
			var td = tr.find("td");	
			$("#deleteRewardModal input[name=rewardId]").val($(td[0]).text());
		});

	</script>
	</body>
</html>