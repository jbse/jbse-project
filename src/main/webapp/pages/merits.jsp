<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta charset="utf-8">
		<meta name="contextPath" content="${contextPath}">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta name="viewport" content="width=device-width" /> 	
	 	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 	
	 	<title>LiveIt Merit System | Merits</title>
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
		<link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/adminStyle.css" />
		<link rel="stylesheet" href="../css/skin-red.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/buttons.dataTables.min.css" />
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.responsive.css">
		<link rel="stylesheet" href="../css/plugins/dataTables/dataTables.tableTools.min.css">
		<link rel="stylesheet" href="../lib/css/bootstrap-datepicker.min.css" />
		<link rel="stylesheet" href="../css/styles.css" />
	</head>
	<div class="modal fade" tabindex="-1" role="dialog" id="addMeritModal">
	    <div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Add another merit</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="form-group">
							<label>Merit Description</label>
				            <textarea class="form-control" name="meritDesc" placeholder="Merit description..."></textarea>
			            </div>
			            <div class="form-group">
							<label>Points</label>
				            <input type="number" class="form-control" name="meritPoints" min="0" step="1" placeholder="Merit points">
			            </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary">Save changes</button>
		            </div>
	            </form>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
<!-- /.modal -->
<!--modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="EditMeritModal">
	    <div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" action="/SoaBaseCode/admin/merits/update">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Edit Merit</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="form-group hidden">
							<label>Merit Description</label>
				            <input type="number" class="form-control" name="meritId" />
			            </div>
		            	<div class="form-group">
							<label>Merit Description</label>
				            <textarea class="form-control" name="meritDesc" placeholder="Merit description..."></textarea>
			            </div>
			            <div class="form-group">
							<label>Points</label>
				            <input type="number" class="form-control" name="meritPoints" min="0" step="1" placeholder="Merit points">
			            </div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary">Save changes</button>
		            </div>
	            </form>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>

	<body class="hold-transition skin-red sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a href="#" class="logo">
					<span class="logo-mini"><b>L</b>IT</span>
					<span class="logo-lg"><b>Live</b>IT</span>
				</a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        				<span class="sr-only">Toggle navigation</span>
      				</a>
      				<div class="navbar-custom-menu">
      					<ul class="nav navbar-nav">
      						<li class="dropdown user user-menu">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				              <span class="fa fa-user"></span>
				              <span class="hidden-xs">${user.fname}</span>
				            </a>
				            <ul class="dropdown-menu">
				              <!-- The user image in the menu -->
				              <li class="user-header">
				                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
				
				                <p>
				                  Admin - Administrator
				                  <small>Member since Nov. 2012</small>
				                </p>
				              </li>
				              <li class="user-footer">
				                <div class="pull-left">
				                  <a href="#" class="btn btn-default btn-flat">Profile</a>
				                </div>
				                <div class="pull-right">
				                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
				                </div>
				              </li>
				            </ul>
				          </li>
      					</ul>
      				</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
				      <ul class="sidebar-menu">
				      	<li class="header">Menu</li>
				        <li><a href="/SoaBaseCode/admin/users"><i class="fa fa-table"></i> <span>Users</span></a></li>
				        <li class="active"><a href="/SoaBaseCode/admin/merits"><i class="fa fa-check-square-o"></i> <span>Merits</span></a></li>
				        <li><a href="/SoaBaseCode/admin/rewards"><i class="fa fa-star"></i> <span>Rewards</span></a></li>
				        <li><a href="/SoaBaseCode/admin/report"><i class="fa fa-database" aria-hidden="true"></i> <span>Report</span></a></li>
				      </ul>
				</section>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Merit list<small></small></h1>
				</section>
				<section class="content">
					<div class="box">
						<div class="box-header with-border">
							 <h3 class="box-title">Merit list table</h3>
							 <div class="box-tools pull-right">
           						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              					<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<c:if test="${flag == 'SUCCESS'}">
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> Merit added successfully.
								</div>
							</c:if>
							<c:if test="${flag == 'UPDATED'}">
								<div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> Merit updated successfully.
								</div>
							</c:if>
							<c:if test="${flag == 'FAILED'}">
								<div class="alert alert-danger alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Oops!</strong> Failed to add merit.
								</div>
							</c:if>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group input-group pull-left" style='width: 250px'>
										<input type="text" class="form-control" name="q" placeholder="Search by id">
										<span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
									</div>
									<button class="lms-btn pull-right form-group" data-toggle="modal" data-target="#addMeritModal">
										<i class="fa fa-plus" aria-hidden="true"></i> Add merit
									</button>
									<div class="clearfix"></div>
									<table id="example2" class="table table-striped table-bordered table-hover">
              							<thead>
              								<tr>
              									<th>ID</th>
              									<th>Description</th>
              									<th>Points</th>
              									<th>Action</th>
              								</tr>
              							</thead>
              							<tbody>
											<c:forEach items="${meritList}" var="m">
											    <tr id="${m.meritId}">
											        <td><c:out value="${m.meritId}"/></td>
											        <td><c:out value="${m.meritDesc}"/></td>
											        <td><c:out value="${m.meritPoints}"/></td>
											        <td>
											        	<button class="btn btn-success EditMeritModal" data-toggle="modal" data-target="#EditMeritModal">
											        		<i class="fa fa-pencil" aria-hidden="true"></i>
											        	</button>
											        	<button class="btn btn-danger">
											        		<i class="fa fa-trash" aria-hidden="true"></i>
											        	</button>
											        </td>
											    </tr>
											</c:forEach>
              							</tbody>
              						</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
			    <div class="pull-right hidden-xs">LiveIT Merit Reward System</div>
			    <strong>Copyright &copy; 2016 <a href="#">UC-MAIN Campus</a>.</strong> All rights reserved.
			</footer>	
	</div>
	
	<script src="../js/jquery-2.2.3.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/app.js"></script>
	<script src="../lib/js/bootstrap-datepicker.min.js"></script>
	
	
	<!-- Datatables -->
	<script src="../js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
	<script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>
	
	<script type="text/javascript">
	$(document).on("click", ".EditMeritModal", function(){
		var tr = $(this).parents("tr");
		var td = tr.find("td");
		$("#EditMeritModal input[name=meritId]").val($(td[0]).text());
		$("#EditMeritModal textarea[name=meritDesc]").val($(td[1]).text());
		$("#EditMeritModal input[name=meritPoints]").val($(td[2]).text());
	});
	</script>
	</body>
</html>