package ph.com.alliance.controller.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/pl")
public class ProjectLeadController {
	@RequestMapping(value="/home",  method=RequestMethod.GET)
	public String loadProjectLeaderView(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		return "projectLeadIndex";
	}
}
