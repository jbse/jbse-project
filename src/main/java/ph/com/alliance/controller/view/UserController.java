package ph.com.alliance.controller.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Example controller class that handles request for the application root.
 * 
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	
    @RequestMapping(value = "/home", method=RequestMethod.GET)
    public String loadProjectLeaderView(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	return "/projectLeadIndex";
    }
}