package ph.com.alliance.controller.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Merit;
import ph.com.alliance.entity.RedeemedReward;
import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.MeritModel;
import ph.com.alliance.model.RedeemedRewardModel;
import ph.com.alliance.model.RewardModel;
import ph.com.alliance.model.UserModel;
import ph.com.alliance.service.DBTransactionTestService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	DBTransactionTestService dbSvc;
	
	@Autowired
	DozerBeanMapper dozerBeanMapper;
	
	/**
     * 
     * @param request
     * @param response
     * @param map
     * @return
     */
	
	
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String viewUserForm(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	HttpSession session = request.getSession();
    	UserModel user = (UserModel) session.getAttribute("user"); 
    	
    	List<User> userList = dbSvc.selectAllUsers();
    	List<UserModel> userModelList = new ArrayList<UserModel>();
		for(User uList : userList) {
			if(uList.getRole().equals("ADMIN"))
				continue;
			userModelList.add(convertToUserModel(uList));
		}
		
    	String flag = request.getParameter("flag");
    	
		request.setAttribute("user", user);
		request.setAttribute("flag", flag);
		request.setAttribute("userList", userModelList);
    	return "adminUserList";
    }
    
    @RequestMapping(value = "/users/create", method = RequestMethod.POST)
    public UserModel saveUser(HttpServletRequest request, HttpServletResponse response, ModelMap map, UserModel u) {    	
    	if(u.getRole().equals("ADMIN")) {
    		try {
				response.sendRedirect(request.getContextPath() + "/admin/users?flag=FAILED");
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	if(!dbSvc.createUser(this.convertToUserEntity(u))) {
    		u = null;
    	}

    	String url = request.getContextPath() + "/admin/users";
    	if(u != null) {
    		url += "?flag=SUCCESS";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return u;
    }
    
    //updateUser
    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public UserModel updateUser(HttpServletRequest request, HttpServletResponse response, ModelMap map, UserModel u) {    	
    	if(u.getRole().equals("ADMIN")) {
    		try {
				response.sendRedirect(request.getContextPath() + "/admin/users?flag=FAILED");
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	User pUser = new User();
    	pUser.setUserId(u.getUserId());
    	User u1 = dbSvc.selectUser(pUser);
    	u1.setFname(u.getFname());
    	u1.setLname(u.getLname());
    	u1.setMname(u.getMname());
    	u1.setEmail(u.getEmail());
    	u1.setGender(u.getGender());
    	u1.setbDate(u.getbDate());
    	u1.setRole(u.getRole());
    	dbSvc.updateUser(u1);
    	
    	String url = request.getContextPath() + "/admin/users";
    	if(u != null) {
    		url += "?flag=EDITED";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return u;
    }
    
  //deleteUser
    @RequestMapping(value = "/users/delete", method = RequestMethod.POST)
    public UserModel deleteUser(HttpServletRequest request, HttpServletResponse response, ModelMap map, UserModel u) {    	
    	if(u.getRole().equals("ADMIN")) {
    		try {
				response.sendRedirect(request.getContextPath() + "/admin/users?flag=FAILED");
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    
    	User pUser = new User();
    	pUser.setUserId(u.getUserId());
    	User u1 = dbSvc.selectUser(pUser);
    	u1.setStatus("INACTIVE");
    	dbSvc.updateUser(u1);
    	String url = request.getContextPath() + "/admin/users";
    	if(u != null) {
    		url += "?flag=DELETED";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return u;
    }
    
  //activateUser
    @RequestMapping(value = "/users/activate", method = RequestMethod.POST)
    public UserModel activateUser(HttpServletRequest request, HttpServletResponse response, ModelMap map, UserModel u) {    	
    	if(u.getRole().equals("ADMIN")) {
    		try {
				response.sendRedirect(request.getContextPath() + "/admin/users?flag=FAILED");
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    
    	User pUser = new User();
    	pUser.setUserId(u.getUserId());
    	User u1 = dbSvc.selectUser(pUser);
    	u1.setStatus("ACTIVE");
    	dbSvc.updateUser(u1);
    	String url = request.getContextPath() + "/admin/users";
    	if(u != null) {
    		url += "?flag=ACTIVATED";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return u;
    }
    
    private int parseInt(String parameter) {
		// TODO Auto-generated method stub
		return 0;
	}

	@RequestMapping(value = "/merits", method = RequestMethod.GET)
	public String showMerits(HttpServletRequest request, HttpServletResponse response, ModelMap map, MeritModel merit) {
    	HttpSession session = request.getSession();
    	UserModel user = (UserModel) session.getAttribute("user");
    	if(user == null) {
    		try {
				response.sendRedirect(request.getContextPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	List<Merit> meritList = dbSvc.selectAllMerits();
    	List<MeritModel> meritModelList = new ArrayList<MeritModel>();
		for(Merit mList : meritList) {
			meritModelList.add(convertToMeritModel(mList));
		}
    	
    	String flag = request.getParameter("flag");
    	
    	request.setAttribute("flag", flag);
    	request.setAttribute("user", user);
    	request.setAttribute("meritList", meritModelList);
    	
	    return "merits";
	}
    @RequestMapping(value = "/merits/update", method = RequestMethod.POST)
    public Merit updateMerit(HttpServletRequest request, HttpServletResponse response, ModelMap map, MeritModel merit){
  
    	Merit meritUp = new Merit();
    	meritUp.setMeritId(Integer.parseInt(request.getParameter("meritId")));
    	
    	Merit u  = dbSvc.selectMerit(meritUp);
    	int points = Integer.parseInt(request.getParameter("meritPoints"));
    	u.setMeritDesc(request.getParameter("meritDesc"));
    	u.setMeritPoints(points);
    	
    	Merit updateMerit = dbSvc.updateMerit(u);
    	
    	String url = request.getContextPath() + "/admin/merits";
    	url += "?flag=UPDATED";
    	
    	try{
    		response.sendRedirect(url);
    		return null;
    	} catch (IOException e){
    		e.printStackTrace();
    	}
    	
    	
    	  
    	
    	return null;
    }
    
    @RequestMapping(value = "/merits", method = RequestMethod.POST)
	public MeritModel saveMerit(HttpServletRequest request, HttpServletResponse response, ModelMap map, MeritModel merit) {
    	if(!dbSvc.createMerit(this.convertToMeritEntity(merit))) {
    		merit = null;
    	}
    	
    	String url = request.getContextPath() + "/admin/merits";
    	if(merit != null) {
    		url += "?flag=SUCCESS";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return null;
	}
    
    @RequestMapping(value = "/rewards", method = RequestMethod.GET)
    public String showRewards(HttpServletRequest request, HttpServletResponse response, ModelMap map, RewardModel reward) {
    	HttpSession session = request.getSession();
    	UserModel user = (UserModel) session.getAttribute("user");
    	if(user == null) {
    		try {
				response.sendRedirect(request.getContextPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	List<Reward> rewardList = dbSvc.selectAllRewards();
    	List<RewardModel> rewardModelList = new ArrayList<RewardModel>();
		for(Reward rList : rewardList) {
			rewardModelList.add(convertToRewardModel(rList));
		}
    	
    	String flag = request.getParameter("flag");
    	
    	request.setAttribute("flag", flag);
    	request.setAttribute("user", user);
    	request.setAttribute("rewardList", rewardModelList);
    	
	    return "rewards";
	}
    
    @RequestMapping(value = "/rewards", method = RequestMethod.POST)
	public RewardModel saveReward(HttpServletRequest request, HttpServletResponse response, ModelMap map, RewardModel rewardModel) {
    	if(!dbSvc.createReward(this.convertToRewardEntity(rewardModel))) {
    		rewardModel = null;
    	}
    	
    	String url = request.getContextPath() + "/admin/rewards";
    	if(rewardModel != null) {
    		url += "?flag=SUCCESS";
    	} else {
    		url += "?flag=FAILED";
    	}
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return null;
	}
    
    @RequestMapping(value = "/rewards/updateReward", method = RequestMethod.POST)
    @ResponseBody
    public Reward updateReward(HttpServletRequest request, HttpServletResponse response, ModelMap map) {

    	Reward reward = new Reward();
    	String rId= request.getParameter("rewardId");
    	reward.setRewardId(Long.parseLong(rId));
    	
    	Reward r = dbSvc.selectReward(reward);
    	int pointsReq = (request.getParameter("pointsReq") == "" ? 0: Integer.parseInt(request.getParameter("pointsReq")));
    	int qty = (request.getParameter("qty") == "" ? 0: Integer.parseInt(request.getParameter("qty")));
    	r.setRewardDesc(request.getParameter("rewardDesc"));
    	r.setPointsReq(pointsReq);
    	r.setQty(qty);
    	
    	Reward updatedReward = dbSvc.updateReward(r);
    	
    	System.out.println("MAPPED USER --- " + updatedReward);
    	String url = request.getContextPath() + "/admin/rewards";
    	url += "?flag=UPDATED";
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return updatedReward;
    }
    
    /**
     * 
     * @param rewardid
     * @return
     */
    @RequestMapping(value = "rewards/deleteReward", method = RequestMethod.POST)
    @ResponseBody
    public Reward deleteReward(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	Reward r = new Reward();
    	String rId = request.getParameter("rewardId");
    	r.setRewardId(Long.parseLong(rId));
    	dbSvc.deleteReward(r);
    	String url = request.getContextPath() + "/admin/rewards";
    	
    	url += "?flag=DELETED";
    	
    	try {
			response.sendRedirect(url);
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return r; 
    }
    
    
    @RequestMapping(value = "/report", method = RequestMethod.GET)
	public String showReports(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	List<RedeemedRewardModel> rrModelList;
    	String keyword = (request.getParameter("q") != null)? request.getParameter("q") : "";
    	if(!keyword.trim().equals("")) {
    		rrModelList = dbSvc.searchRedeemedReward(keyword);
    		request.setAttribute("length", rrModelList.size());
    		request.setAttribute("q", keyword);
    	} else {
        	rrModelList = dbSvc.selectAllRedeemedReward();
    	}
    	request.setAttribute("redeemedList", rrModelList);
	    return "reports";
	}
    
    @RequestMapping(value = "/api/get-redeemed-list-by-dates", method = RequestMethod.GET)
	@ResponseBody
	public List<RedeemedRewardModel> getAllRedeemedListByDates(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		List<RedeemedRewardModel> rrModelList = null;
		String startDate = request.getParameter("startDate");
    	String endDate = request.getParameter("endDate");
		rrModelList = dbSvc.selectRedeemedRewardByDates(startDate, endDate);
		
		return rrModelList;
	}
    
    private RedeemedRewardModel convertToRRModel(RedeemedReward rr) {	
    	RedeemedRewardModel rrModel = null;

    	if (rr != null) {
    		rrModel = dozerBeanMapper.map(rr, RedeemedRewardModel.class);
    	}
    	
    	return rrModel;
    }
    
    private RedeemedReward convertToRREntity (RedeemedRewardModel rrModel) {
    	RedeemedReward rr = null;
    	
    	if (rrModel != null) {
    		rr = dozerBeanMapper.map(rrModel, RedeemedReward.class);
    	}
    	
    	return rr;
    }
    
    private MeritModel convertToMeritModel (Merit merit) {	
    	MeritModel meritModel = null;

    	if (merit != null) {
    		meritModel = dozerBeanMapper.map(merit, MeritModel.class);
    	}
    	
    	return meritModel;
    }
    
    private Merit convertToMeritEntity (MeritModel meritModel) {
    	Merit merit = null;
    	
    	if (meritModel != null) {
    		merit = dozerBeanMapper.map(meritModel, Merit.class);
    	}
    	
    	return merit;
    }
    
    private RewardModel convertToRewardModel (Reward reward) {	
    	RewardModel rewardModel = null;

    	if (reward != null) {
    		rewardModel = dozerBeanMapper.map(reward, RewardModel.class);
    	}
    	
    	return rewardModel;
    }
    
    private Reward convertToRewardEntity (RewardModel rewardModel) {
    	Reward reward = null;
    	
    	if (rewardModel != null) {
    		reward = dozerBeanMapper.map(rewardModel, Reward.class);
    	}
    	
    	return reward;
    }
    
    private UserModel convertToUserModel (User pUser) {	
    	UserModel userModel = null;

    	if (pUser != null) {
    		userModel = dozerBeanMapper.map(pUser, UserModel.class);
    	}
    	
    	return userModel;
    }
    
    private User convertToUserEntity (UserModel pUserModel) {
    	User u = null;
    	
    	if (pUserModel != null) {
    		u = dozerBeanMapper.map(pUserModel, User.class);
    	}
    	
    	return u;
    }
}
