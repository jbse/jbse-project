package ph.com.alliance.controller.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.model.UserModel;

/**
 * Example controller class that handles request for the application root.
 * 
 *
 */
@Controller
@RequestMapping("/")
public class LoginController {
	
    @RequestMapping(method=RequestMethod.GET)
    public String loadMainMenuIndex(HttpServletRequest request, HttpServletResponse response, ModelMap map){

    	String INVALID_CREDENTIALS = "These credentials do not match our record.";
    	String isError = request.getParameter("error");
    	
    	HttpSession session = request.getSession();
    	try {
    		if(session.getAttribute("user") != null) {
    			UserModel user = (UserModel) session.getAttribute("user");
		    	if(user.getRole().equals("ADMIN")) {
					response.sendRedirect(request.getContextPath() + "/admin/users");
				}
		    	else if(user.getRole().equals("PL")) {
		    		response.sendRedirect(request.getContextPath() + "/pl/home");
		    	}
		    	else if(user.getRole().equals("USER")) {
		    		response.sendRedirect(request.getContextPath() + "/user/home2");
		    	}
    		}
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	if(isError != null) {
    		if(isError.equals("INVALID_CREDENTIALS")) {
    			request.setAttribute("error", INVALID_CREDENTIALS);
    		}
    	}
		return "/index";
    }
}
