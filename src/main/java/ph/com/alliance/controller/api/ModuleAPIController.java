package ph.com.alliance.controller.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.UserModel;
import ph.com.alliance.service.DBTransactionTestService;

/**
 * Controller class used to hadle api requests.
 * All requests that falls through /api/* servlet mapping goes through here.
 * 
 */
@Controller
public class ModuleAPIController {
	
	@Autowired
	DBTransactionTestService dbSvc;
	
	@Autowired
	DozerBeanMapper dozerBeanMapper;
	
	
	
	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	@ResponseBody
	public List<UserModel> getAll(){
		List<User> userList = dbSvc.selectAllUsers();
		List<UserModel> userModelList = new ArrayList<UserModel>();
		
		for(User u : userList){
			userModelList.add(convertToModel(u));
		}
		
		return userModelList;
	}

    /**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public void loginUser(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	User u = new User();
    	String email = request.getParameter("email");
    	String password = request.getParameter("password");
    	
    	u.setEmail(email);
    	u.setPassword(password);
    	
    	List<User> userList = dbSvc.verifyLogin(u);
    	UserModel currentUser = new UserModel();
    	String uri = "";

    	try {
	    	if(CollectionUtils.isEmpty(userList)) {
	    		uri = "/?error=INVALID_CREDENTIALS";
	    	} else {
	    		currentUser = convertToModel(userList.get(0));
	    		request.getSession().setAttribute("user", currentUser);
	    		if(currentUser.getRole().equals("ADMIN")) {
	    			uri = "/admin/users";	
	    		} else if(currentUser.getRole().equals("PL")){
	    			uri = "/pl/home";
	    		}
	    		else if((currentUser.getRole().equals("USER"))){
	    			uri = "/user/home";
	    		}
	    	}
	    	response.sendRedirect(request.getContextPath() + uri);
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    @ResponseBody

    public UserModel saveUser(HttpServletRequest request, HttpServletResponse response, ModelMap map, UserModel u) {
    	if(!dbSvc.createUser(this.convertToEntity(u))) {
    		u = null;
    	}
 		System.out.println("MAPPED USER --- " + this.convertToEntity(u));
    	return u;
    }
    
    /**
     * 
     * @param uid
     * @return
     */
    @RequestMapping(value = "/searchUser/{uid}", method = RequestMethod.GET)
    @ResponseBody
    public UserModel searchUser(@PathVariable("uid") int uid) {
    	User u = new User();
    	
    	u.setUserId(uid);
    	
    	return convertToModel(dbSvc.selectUser(u));
    }
    
    /**
     * 
     * @return
     */
    @RequestMapping(value = "/searchAllUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<UserModel> searchAllUsers() {
    	List<User> userList = dbSvc.selectAllUsers();
    	List<UserModel> userModelList = new ArrayList<UserModel>();
    	
    	for(User u : userList) {
    		userModelList.add(convertToModel(u));
    	}
    	 	
    	return userModelList;
    }
    
        
    


	/**
	 * 
	 * @param request
	 * @param response
	 * @param map
	 * @return
	 */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public void logoutUser(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
    	request.getSession().invalidate();
    	try {
			response.sendRedirect(request.getRequestURL() + "/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * This is a sample object mapper.
     * Entity to model mapping can be handled by the class constructor itself, or
     * see convertToEntity function for another type of mapping.
     * 
     * @param pUser
     * @return UserModel
     */
    private UserModel convertToModel (User pUser) {	
    	UserModel userModel = null;

    	if (pUser != null) {
    		userModel = dozerBeanMapper.map(pUser, UserModel.class);
    	}
    	
    	return userModel;
    }
    
    /**
     * This is a sample object mapper.
     * Model to entity mapping can be explicitly done via setters, or
     * see convertToModel function for mapping using constructor
     * 
     * @param pUserModel
     * @return
     */
    private User convertToEntity (UserModel pUserModel) {
    	User u = null;
    	
    	if (pUserModel != null) {
    		u = dozerBeanMapper.map(pUserModel, User.class);
    	}
    	
    	return u;
    }
}
