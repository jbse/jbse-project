package ph.com.alliance.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import ph.com.alliance.model.UserModel;


public class UserModuleInterceptor extends HandlerInterceptorAdapter {

	public UserModuleInterceptor() {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
	    throws Exception {
		HttpSession session = request.getSession();
		String uri = request.getContextPath() + request.getRequestURI();
		
		if(session.getAttribute("user") != null) {
			UserModel user = (UserModel) session.getAttribute("user");			
			if(uri.indexOf("admin") != -1 && !user.getRole().equals("ADMIN")) {
				uri = request.getContextPath() + "/restricted";
				response.sendRedirect(uri);
				return false;
			}
			else if(uri.indexOf("pl") != -1 && !user.getRole().equals("PL")) {
				uri = request.getContextPath() + "/restricted";
				response.sendRedirect(uri);
				return false;
			}
			else if(uri.indexOf("user") != -1 && user.getRole().equals("USER")) {
				uri = request.getContextPath() + "/restricted";
				response.sendRedirect(uri);
				return false;
			}
			return true;
		} else {
			
			uri = request.getContextPath() + "/restricted";
		}
		
		response.sendRedirect(uri);
		return false;
	}
	
	@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
	}
}

