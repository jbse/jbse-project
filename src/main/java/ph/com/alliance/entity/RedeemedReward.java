package ph.com.alliance.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the redeemed_rewards database table.
 * 
 */
@Entity
@Table(name="redeemed_reward")
@NamedQuery(name="RedeemedReward.findAll", query="SELECT r FROM RedeemedReward r")
public class RedeemedReward implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int rrId;

	private Timestamp dateRedeemed;

	private int fkRewardId;

	private int fkUserId;

	private int pointsReq;

	private short qty;

	public RedeemedReward() {
	}

	public int getRrId() {
		return this.rrId;
	}

	public void setRrId(int rrId) {
		this.rrId = rrId;
	}

	public Timestamp getDateRedeemed() {
		return this.dateRedeemed;
	}

	public void setDateRedeemed(Timestamp dateRedeemed) {
		this.dateRedeemed = dateRedeemed;
	}

	public int getFkRewardId() {
		return this.fkRewardId;
	}

	public void setFkRewardId(int fkRewardId) {
		this.fkRewardId = fkRewardId;
	}

	public int getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}

	public int getPointsReq() {
		return this.pointsReq;
	}

	public void setPointsReq(int pointsReq) {
		this.pointsReq = pointsReq;
	}

	public short getQty() {
		return this.qty;
	}

	public void setQty(short qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "RedeemedReward [rrId=" + rrId + ", dateRedeemed="
				+ dateRedeemed + ", fkRewardId=" + fkRewardId + ", fkUserId="
				+ fkUserId + ", pointsReq=" + pointsReq + ", qty=" + qty + "]";
	}

}