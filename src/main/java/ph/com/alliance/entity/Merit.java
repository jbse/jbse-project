package ph.com.alliance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="merit")
public class Merit {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="meritId")
	private long meritId;
	
	@Column(name="meritDesc")
	private String meritDesc;
	
	@Column(name="meritPoints")
	private long meritPoints;
	
	@Column(name="created_at")
	private String createDate;
	
	@Column(name="updated_at")
	private String updateDate;

	public long getMeritId() {
		return meritId;
	}

	public void setMeritId(long meritId) {
		this.meritId = meritId;
	}

	public String getMeritDesc() {
		return meritDesc;
	}

	public void setMeritDesc(String meritDesc) {
		this.meritDesc = meritDesc;
	}

	public long getMeritPoints(){
		return meritPoints;
	}

	public void setMeritPoints(long meritPoints) {
		this.meritPoints = meritPoints;
	}

	public String getcreateDate() {
		return createDate;
	}

	public void setcreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Merit [meritId=" + meritId + ", meritDesc=" + meritDesc
				+ ", meritPoints=" + meritPoints + ", createDate="
				+ createDate + ", updateDate=" + updateDate + "]";
	}
	
	
	
	
	
	
}
