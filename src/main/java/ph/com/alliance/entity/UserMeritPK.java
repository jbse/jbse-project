package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_merit database table.
 * 
 */
@Embeddable
public class UserMeritPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int fkMeritId;

	private int fkUserId;

	public UserMeritPK() {
	}
	public int getFkMeritId() {
		return this.fkMeritId;
	}
	public void setFkMeritId(int fkMeritId) {
		this.fkMeritId = fkMeritId;
	}
	public int getFkUserId() {
		return this.fkUserId;
	}
	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserMeritPK)) {
			return false;
		}
		UserMeritPK castOther = (UserMeritPK)other;
		return 
			(this.fkMeritId == castOther.fkMeritId)
			&& (this.fkUserId == castOther.fkUserId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fkMeritId;
		hash = hash * prime + this.fkUserId;
		
		return hash;
	}
}