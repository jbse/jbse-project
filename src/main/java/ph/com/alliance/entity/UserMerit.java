package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_merit database table.
 * 
 */
@Entity
@Table(name="user_merit")
@NamedQuery(name="UserMerit.findAll", query="SELECT u FROM UserMerit u")
public class UserMerit implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserMeritPK id;

	private String dateGiven;

	private int fkPlId;

	private int meritPoints;

	public UserMerit() {
	}

	public UserMeritPK getId() {
		return this.id;
	}

	public void setId(UserMeritPK id) {
		this.id = id;
	}

	public String getDateGiven() {
		return this.dateGiven;
	}

	public void setDateGiven(String dateGiven) {
		this.dateGiven = dateGiven;
	}

	public int getFkPlId() {
		return this.fkPlId;
	}

	public void setFkPlId(int fkPlId) {
		this.fkPlId = fkPlId;
	}

	public int getMeritPoints() {
		return this.meritPoints;
	}

	public void setMeritPoints(int meritPoints) {
		this.meritPoints = meritPoints;
	}

}