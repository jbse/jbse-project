package ph.com.alliance.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reward")
public class Reward {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="rewardId")
	private long rewardId;
	
	@Column(name="rewardDesc")
	private String rewardDesc;
	
	@Column(name="pointsReq")
	private long pointsReq;
	
	@Column(name="qty")
	private int qty;
	
	@Column(name="status")
	private String status;
	
	@Column(name="validity")
	private String validity;
	
	@Column(name="created_at")
	private String createDate;
	
	@Column(name="updated_at")
	private String updateDate;

	public long getRewardId() {
		return rewardId;
	}

	public void setRewardId(long rewardId) {
		this.rewardId = rewardId;
	}

	public String getRewardDesc() {
		return rewardDesc;
	}

	public void setRewardDesc(String rewardDesc) {
		this.rewardDesc = rewardDesc;
	}

	public long getPointsReq() {
		return pointsReq;
	}

	public void setPointsReq(long pointsReq) {
		this.pointsReq = pointsReq;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Rewards [rewardId=" + rewardId + ", rewardDesc=" + rewardDesc
				+ ", pointsReq=" + pointsReq + ", qty=" + qty + ", status="
				+ status + ", validity=" + validity + ", createDate="
				+ createDate + ", updateDate=" + updateDate + "]";
	}
	
	
	
	
}
