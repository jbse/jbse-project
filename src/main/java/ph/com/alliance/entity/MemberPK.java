package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the members database table.
 * 
 */
@Embeddable
public class MemberPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private int fkUserId;

	private int fkPlId;

	public MemberPK() {
	}
	public int getFkUserId() {
		return this.fkUserId;
	}
	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}
	public int getFkPlId() {
		return this.fkPlId;
	}
	public void setFkPlId(int fkPlId) {
		this.fkPlId = fkPlId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MemberPK)) {
			return false;
		}
		MemberPK castOther = (MemberPK)other;
		return 
			(this.fkUserId == castOther.fkUserId)
			&& (this.fkPlId == castOther.fkPlId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fkUserId;
		hash = hash * prime + this.fkPlId;
		
		return hash;
	}
}