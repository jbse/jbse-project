package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.RedeemedReward;
import ph.com.alliance.entity.User;
import ph.com.alliance.entity.Reward;
import ph.com.alliance.model.RedeemedRewardModel;

/**
 * 
 * 
 */
public interface RedeemedRewardDao {
	/**
	 * 
	 * @param puser
	 * @param reward
	 * @return
	 */
	public boolean redeemReward(EntityManager pEM, User pUser, Reward reward);
	public List<RedeemedRewardModel> selectAllRedeemedReward(EntityManager pEM);
	public List<RedeemedRewardModel> selectRedeemedRewardByDates(EntityManager pEM, String startDate, String endDate);
	public List<RedeemedRewardModel> searchRedeemedReward(EntityManager pEM, String keyword);
		
}
