package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Merit;
import ph.com.alliance.entity.User;

/**
 * 
 * 
 */
public interface MeritDao {
	
	public boolean createMerit(EntityManager pEM, Merit merit);
	
	public Merit updateMerit(EntityManager pEM, Merit merit);
	
	public int deleteMerit(EntityManager pEM, Merit merit);
	
	public Merit selectMerit(EntityManager pEM, long pMeritId);

	public List<Merit> selectAllMerits(EntityManager pEM);
	
}
