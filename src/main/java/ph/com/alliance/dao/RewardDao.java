package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Reward;

/**
 * 
 * 
 */
public interface RewardDao {
	
	public boolean createReward(EntityManager pEM, Reward reward);
	
	public Reward updateReward(EntityManager pEM, Reward reward);
	
	public int deleteReward(EntityManager pEM, Reward reward);
	
	public Reward selectReward(EntityManager pEM, long pRewardId);

	public List<Reward> selectAllRewards(EntityManager pEM);
	
}
