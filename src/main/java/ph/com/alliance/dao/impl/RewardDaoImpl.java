package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.RewardDao;
import ph.com.alliance.entity.Reward;

/**
 * Sample data access object implementation using Java Persistence API.
 * 
 *
 */
@Repository("rewardDao")
public class RewardDaoImpl implements RewardDao {
	
	/**
	 * TO DO:
	 * 1. Create Own Exception Class (ie. MyException class)
	 * 2. Let dao handle all hibernate/sql related exceptions and throw MyException to service layer 
	 * 		so that service layer will only handle MyException should there be errors
	 * 3. Every dao function should throw MyException
	 * 4. Should dao handle NullPointerExceptions?
	 */

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#createUser(javax.persistence.EntityManager, ph.com.alliance.entity.User)
	 */
	@Override
	public boolean createReward(EntityManager pEM, Reward reward) {	
		boolean success = true;
				
		try {
			pEM.persist(reward);
		} catch (EntityExistsException ee) {
			ee.getMessage();
			success = false;
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}


	@Override
	public Reward updateReward(EntityManager pEM, Reward pReward) {
		Reward reward = null;
		try {
			reward = pEM.merge(pReward);
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
		}
		
		return reward;
	}

	
	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.RewardDao#deleteReward(javax.persistence.EntityManager, ph.com.alliance.entity.Reward)
	 */
	@Override
	public int deleteReward(EntityManager pEM, Reward reward) {
		// TODO Auto-generated method stub
		Reward r = pEM.find(Reward.class, reward.getRewardId());
		pEM.getTransaction().begin();
		pEM.remove(r);
		pEM.getTransaction().commit();
		return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.RewardDao#selectReward(javax.persistence.EntityManager, java.lang.String)
	 */
	@Override
	public Reward selectReward(EntityManager pEM, long pRewardId) {
		Reward reward = null;
		
		try {
			
			reward = pEM.find(Reward.class, pRewardId);
						
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		}
			
		return reward;
	}

	@Override
	public List<Reward> selectAllRewards(EntityManager pEM) {
		// TODO Auto-generated method stub
		CriteriaBuilder cb = pEM.getCriteriaBuilder();
		CriteriaQuery<Reward> cq = cb.createQuery(Reward.class);
		Root<Reward> rewardRoot = cq.from(Reward.class);
		cq.select(rewardRoot);
		try {
			return pEM.createQuery(cq).getResultList();
		} catch (Exception e) {
			System.err.println("ERROR ----------------- ");
			e.printStackTrace();
			return null;
		}
	}

}
