package ph.com.alliance.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TransactionRequiredException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.RedeemedRewardDao;
import ph.com.alliance.dao.UserDao;
import ph.com.alliance.entity.RedeemedReward;
import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.RedeemedRewardModel;

/**
 * Sample data access object implementation using Java Persistence API.
 * 
 *
 */
@Repository("redeemedRewardDao")
public class RedeemedRewardDaoImpl implements RedeemedRewardDao {
	
	/**
	 * TO DO:
	 * 1. Create Own Exception Class (ie. MyException class)
	 * 2. Let dao handle all hibernate/sql related exceptions and throw MyException to service layer 
	 * 		so that service layer will only handle MyException should there be errors
	 * 3. Every dao function should throw MyException
	 * 4. Should dao handle NullPointerExceptions?
	 */

	@Override
	public boolean redeemReward(EntityManager pEM, User pUser, Reward reward) {	
		boolean success = true;
//		try {	
//			pEM.persist(pUser);
//		} catch (EntityExistsException ee) {
//			ee.getMessage();
//			success = false;
//		} catch (IllegalArgumentException iae) {
//			iae.getMessage();
//			success = false;
//		} catch (TransactionRequiredException trxe) {
//			trxe.getMessage();
//			success = false;
//		}
		return success;
	}

	@Override
	public List<RedeemedRewardModel> selectAllRedeemedReward(EntityManager pEM) {
		// TODO Auto-generated method stub
		List<RedeemedRewardModel> rrModelList = new ArrayList<RedeemedRewardModel>();
		
		StringBuilder rrQuery = new StringBuilder("SELECT u, rr, rwrd"
				+ " FROM User u, RedeemedReward rr, Reward rwrd"
				+ " WHERE rr.fkRewardId = rwrd.rewardId AND rr.fkUserId = u.userId");
		
		Query query = pEM.createQuery(rrQuery.toString());
		List<Object[]> list = query.getResultList();
		
		rrModelList = getRModelListFromObj(list);
		return rrModelList;
	}
	
	@Override
	public List<RedeemedRewardModel> searchRedeemedReward(EntityManager pEM, String keyword) {
		// TODO Auto-generated method stub
		List<RedeemedRewardModel> rrModelList = new ArrayList<RedeemedRewardModel>();
		
		StringBuilder rrQuery = new StringBuilder("SELECT u, rr, rwrd"
				+ " FROM User u, RedeemedReward rr, Reward rwrd"
				+ " WHERE rr.fkRewardId = rwrd.rewardId AND rr.fkUserId = u.userId"
				+ " AND (u.fname LIKE :keyword OR u.lname LIKE :keyword"
				+ " OR rwrd.rewardDesc LIKE :keyword"
				+ " OR rr.pointsReq LIKE :keyword"
				+ " OR rr.qty LIKE :keyword)");
		
		Query query = pEM.createQuery(rrQuery.toString());
		query.setParameter("keyword", "%"+keyword+"%");
		
		List<Object[]> list = query.getResultList();
		
		rrModelList = getRModelListFromObj(list);

		return rrModelList;
	}

	@Override
	public List<RedeemedRewardModel> selectRedeemedRewardByDates(EntityManager pEM, String startDate, String endDate) {
		List<RedeemedRewardModel> rrModelList = new ArrayList<RedeemedRewardModel>();
		
		StringBuilder rrQuery = new StringBuilder("SELECT u, rr, rwrd"
				+ " FROM User u, RedeemedReward rr, Reward rwrd"
				+ " WHERE rr.fkRewardId = rwrd.rewardId AND rr.fkUserId = u.userId"
				+ " AND date(rr.dateRedeemed) >= :stDate"
				+ " AND date(rr.dateRedeemed) <= :enDate");
		Query query = pEM.createQuery(rrQuery.toString());
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date stDate = new Date(0);
		Date enDate = new Date(0);
		try {
			stDate = (Date) sdf.parse(startDate);
			enDate = (Date) sdf.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		query.setParameter("stDate", stDate, TemporalType.DATE);
		query.setParameter("enDate", enDate, TemporalType.DATE);
		List<Object[]> list = query.getResultList();
		rrModelList = getRModelListFromObj(list);
		return rrModelList;
	}
	
	public List<RedeemedRewardModel> getRModelListFromObj(List<Object[]> list) {
		List<RedeemedRewardModel> rrModelList = new ArrayList<RedeemedRewardModel>();

		for(Object[] tuple : list) {
			RedeemedReward rr = (RedeemedReward) tuple[1];
			User u = (User) tuple[0];
			Reward r = (Reward) tuple[2];
			
			RedeemedRewardModel rrModel = new RedeemedRewardModel(rr.getRrId(), rr.getDateRedeemed(), (int) r.getRewardId(), (int) u.getUserId(), rr.getPointsReq(), rr.getQty(), u, r);
			rrModelList.add(rrModel);
		}
		
		return rrModelList;
	}

}
