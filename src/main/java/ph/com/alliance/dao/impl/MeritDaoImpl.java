package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.MeritDao;
import ph.com.alliance.dao.UserDao;
import ph.com.alliance.entity.Merit;
import ph.com.alliance.entity.User;

/**
 * Sample data access object implementation using Java Persistence API.
 * 
 *
 */
@Repository("meritDao")
public class MeritDaoImpl implements MeritDao {
	
	/**
	 * TO DO:
	 * 1. Create Own Exception Class (ie. MyException class)
	 * 2. Let dao handle all hibernate/sql related exceptions and throw MyException to service layer 
	 * 		so that service layer will only handle MyException should there be errors
	 * 3. Every dao function should throw MyException
	 * 4. Should dao handle NullPointerExceptions?
	 */

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#createUser(javax.persistence.EntityManager, ph.com.alliance.entity.User)
	 */
	@Override
	public boolean createMerit(EntityManager pEM, Merit merit) {	
		boolean success = true;
				
		try {
			pEM.persist(merit);
		} catch (EntityExistsException ee) {
			ee.getMessage();
			success = false;
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}


	@Override
	public Merit updateMerit(EntityManager pEM, Merit pMerit) {
		Merit merit = null;
		try {
			merit = pEM.merge(pMerit);
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
		}
		
		return merit;
	}


	@Override
	public int deleteMerit(EntityManager pEM, Merit merit) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Merit selectMerit(EntityManager pEM, long pMeritId) {
		Merit merit = null;
		
		try {
			
			merit = pEM.find(Merit.class, pMeritId);
						
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		}
			
		return merit;
		
	}

	@Override
	public List<Merit> selectAllMerits(EntityManager pEM) {
		// TODO Auto-generated method stub
		CriteriaBuilder cb = pEM.getCriteriaBuilder();
		CriteriaQuery<Merit> cq = cb.createQuery(Merit.class);
		Root<Merit> meritRoot = cq.from(Merit.class);
		cq.select(meritRoot);
		try {
			return pEM.createQuery(cq).getResultList();
		} catch (Exception e) {
			System.err.println("ERROR ----------------- ");
			e.printStackTrace();
			return null;
		}
	}

}
