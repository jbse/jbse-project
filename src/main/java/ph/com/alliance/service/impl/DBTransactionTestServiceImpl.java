package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.MeritDao;
import ph.com.alliance.dao.RedeemedRewardDao;
import ph.com.alliance.dao.RewardDao;
import ph.com.alliance.dao.UserDao;
import ph.com.alliance.dao.impl.UserDaoImpl;
import ph.com.alliance.entity.Merit;
import ph.com.alliance.entity.RedeemedReward;
import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.RedeemedRewardModel;
import ph.com.alliance.service.DBTransactionTestService;

/**
 * Example service implementation that handles database transaction.
 * Database transaction starts in this layer of the application, and it also ends here. 
 *
 */

@Service("dBTransactionService")
public class DBTransactionTestServiceImpl implements DBTransactionTestService {
	
	@Autowired
	private UserDao userDao;

	@Autowired
	private MeritDao meritDao;
	
	@Autowired
	private RewardDao rewardDao;
	
	@Autowired
	private RedeemedRewardDao rrDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Override
	public boolean createUser(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		try {
			result = userDao.createUser(em, pUser);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public User updateUser(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		User result = null;
		
		em.getTransaction().begin();
		
		try {
			result = userDao.updateUser(em, pUser);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public void deleteUser(User pUser) {
		// TODO Auto-generated method stub		
	}

	@Override
	public User selectUser(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		return userDao.selectUser(em, pUser.getUserId());
	}
	
	@Override
	public List<User> verifyLogin(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		return userDao.verifyLogin(em, pUser.getEmail(), pUser.getPassword());
	}

	@Override
	public List<User> selectUsers(String pKey) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public List<User> selectAllUsers() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<User> userList = null;
		
		try {
			userList = userDao.selectAllUsers(em);
		} catch (Exception e) {
			System.out.print("Error: " + e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return userList;
	}
	


	@Override
	public boolean createMerit(Merit merit) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		try {
			result = meritDao.createMerit(em, merit);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}
	
	@Override
	public List<RedeemedRewardModel> selectAllRedeemedReward() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<RedeemedRewardModel> redeemedList = null;
		try {
			redeemedList = rrDao.selectAllRedeemedReward(em);
		} catch (Exception e) {
			System.out.print("Error: "+e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return redeemedList;
	}
	
	@Override
	public List<RedeemedRewardModel> selectRedeemedRewardByDates(String startDate, String endDate) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<RedeemedRewardModel> redeemedList = null;
		try {
			redeemedList = rrDao.selectRedeemedRewardByDates(em, startDate, endDate);
		} catch (Exception e) {
			System.out.print("Error: "+e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return redeemedList;
	}

	@Override
	public List<Merit> selectAllMerits() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Merit> meritList = null;
		try {
			meritList = meritDao.selectAllMerits(em);
		} catch (Exception e) {
			System.out.print("Error: "+e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return meritList;
	}

	@Override
	public List<Reward> selectAllRewards() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Reward> rewardList = null;
		try {
			rewardList = rewardDao.selectAllRewards(em);
		} catch (Exception e) {
			System.out.print("Error: "+e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return rewardList;
	}

	@Override
	public boolean createReward(Reward reward) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		em.getTransaction().begin();
		try {
			result = rewardDao.createReward(em, reward);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Merit updateMerit(Merit pMerit) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Merit result = null;
		result = meritDao.updateMerit(em, pMerit);
	}
	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.service.DBTransactionTestService#deleteReward(ph.com.alliance.entity.Reward)
	 */
	@Override
	public void deleteReward(Reward reward) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		rewardDao.deleteReward(em, reward);
	}


	@Override
	public Reward updateReward(Reward reward) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		Reward result = null;
		
		em.getTransaction().begin();
		
		try {
			result = rewardDao.updateReward(em, reward);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public Merit selectMerit(Merit pMerit) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		return meritDao.selectMerit(em, pMerit.getMeritId());
		//return meritDao;
		
		//return null;
	}

	public List<RedeemedRewardModel> searchRedeemedReward(String keyword) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<RedeemedRewardModel> redeemedList = null;
		try {
			redeemedList = rrDao.searchRedeemedReward(em, keyword);
		} catch (Exception e) {
			System.out.print("Error: "+e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return redeemedList;
	}
	
	public Reward selectReward(Reward reward) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		return rewardDao.selectReward(em, reward.getRewardId());
	}
}
