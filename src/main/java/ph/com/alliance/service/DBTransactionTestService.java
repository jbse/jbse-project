package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Merit;
import ph.com.alliance.entity.RedeemedReward;
import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.RedeemedRewardModel;

/**
 * 
 * 
 */
public interface DBTransactionTestService {
	
	public boolean createUser(User pUser);
	
	public User updateUser(User pUser);
	
	public void deleteUser(User pUser);
	
	public User selectUser(User pUser);
	
	public List<User> selectUsers(String pKey);
	
	public List<User> verifyLogin(User pUser);
	
	public List<User> selectAllUsers();

	public List<RedeemedRewardModel> selectAllRedeemedReward();

	public Merit updateMerit(Merit pMerit);
	
	public Merit selectMerit(Merit pMerit);
	
	boolean createMerit(Merit merit);
	
	//boolean updateMerit(Merit merit);
	
	public List<Merit> selectAllMerits();

	public List<Reward> selectAllRewards();

	public boolean createReward(Reward convertToRewardEntity);

	List<RedeemedRewardModel> selectRedeemedRewardByDates(String startDate,
			String endDate);

	public List<RedeemedRewardModel> searchRedeemedReward(String keyword);
	
	public void deleteReward(Reward reward);

	public Reward updateReward(Reward convertToRewardEntity);

	public Reward selectReward(Reward reward);
	
}
