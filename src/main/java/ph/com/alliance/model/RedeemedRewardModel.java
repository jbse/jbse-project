package ph.com.alliance.model;

import java.io.Serializable;

import javax.persistence.*;

import ph.com.alliance.entity.Reward;
import ph.com.alliance.entity.User;

import java.sql.Timestamp;


/**
 * The persistent class for the redeemed_rewards database table.
 * 
 */

public class RedeemedRewardModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int rrId;
	private Timestamp dateRedeemed;
	private int fkRewardId;
	private int fkUserId;
	private int pointsReq;
	private short qty;
	private User user;
	private Reward reward;

	public RedeemedRewardModel(int rrId, Timestamp dateRedeemed,
			int fkRewardId, int fkUserId, int pointsReq, short qty, User user,
			Reward reward) {
		super();
		this.rrId = rrId;
		this.dateRedeemed = dateRedeemed;
		this.fkRewardId = fkRewardId;
		this.fkUserId = fkUserId;
		this.pointsReq = pointsReq;
		this.qty = qty;
		this.user = user;
		this.reward = reward;
	}

	public int getRrId() {
		return this.rrId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}

	public void setRrId(int rrId) {
		this.rrId = rrId;
	}
	
	public Timestamp getDateRedeemed() {
		return this.dateRedeemed;
	}

	public void setDateRedeemed(Timestamp dateRedeemed) {
		this.dateRedeemed = dateRedeemed;
	}

	public int getFkRewardId() {
		return this.fkRewardId;
	}

	public void setFkRewardId(int fkRewardId) {
		this.fkRewardId = fkRewardId;
	}

	public int getFkUserId() {
		return this.fkUserId;
	}

	public void setFkUserId(int fkUserId) {
		this.fkUserId = fkUserId;
	}

	public int getPointsReq() {
		return this.pointsReq;
	}

	public void setPointsReq(int pointsReq) {
		this.pointsReq = pointsReq;
	}

	public short getQty() {
		return this.qty;
	}

	public void setQty(short qty) {
		this.qty = qty;
	}
}