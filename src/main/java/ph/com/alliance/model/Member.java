package ph.com.alliance.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the members database table.
 * 
 */
public class Member implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MemberPK id;

	public Member() {
	}

	public MemberPK getId() {
		return this.id;
	}

	public void setId(MemberPK id) {
		this.id = id;
	}

}