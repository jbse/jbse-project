package ph.com.alliance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class MeritModel {
	
	private static final long serialVersionUID = 1L;
	

	private long meritId;
	

	private String meritDesc;
	

	private long meritPoints;
	

	private String createDate;
	

	private String updateDate;

	public long getMeritId() {
		return meritId;
	}

	public void setMeritId(long meritId) {
		this.meritId = meritId;
	}

	public String getMeritDesc() {
		return meritDesc;
	}

	public void setMeritDesc(String meritDesc) {
		this.meritDesc = meritDesc;
	}

	public long getMeritPoints(){
		return meritPoints;
	}

	public void setMeritPoints(long meritPoints) {
		this.meritPoints = meritPoints;
	}

	public String getcreateDate() {
		return createDate;
	}

	public void setcreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Merit [meritId=" + meritId + ", meritDesc=" + meritDesc
				+ ", meritPoints=" + meritPoints + ", createDate="
				+ createDate + ", updateDate=" + updateDate + "]";
	}
	
	
	
	
	
	
}
